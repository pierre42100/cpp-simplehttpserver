QT += widgets network

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    server.cpp

FORMS += \
    mainwindow.ui

HEADERS += \
    mainwindow.h \
    server.h

RESOURCES += \
    ressources.qrc
