#pragma once

#include <QMainWindow>

#include "server.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QString currDir();
    int currPort();

private slots:
    void on_browseButton_clicked();

    void on_actionQuit_triggered();

    void updateUI(bool serverIsStarted);

    void on_startButton_clicked();

    void onError(const QString &msg);

    void on_stopButton_clicked();

private:
    Ui::MainWindow *ui;
    Server *mServer = nullptr;
};
