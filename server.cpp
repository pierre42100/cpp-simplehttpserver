#include <QBuffer>
#include <QDir>
#include <QTcpServer>
#include <QTcpSocket>
#include <QFileInfo>
#include <QMimeDatabase>
#include <QMimeType>
#include <QUrl>

#include "server.h"

#define ASSETS_PATH "__simpleHTTPServerAssets__/"
#define ASSETS_URL "/" ASSETS_PATH

#define BUFF_SIZE 1000

#define DEFAULT_STYLESHEET "<link rel=\"stylesheet\" href=\"" ASSETS_URL "style.css\" />"

Server::Server(QObject *parent) : QObject(parent)
{

}

void Server::start(int port, const QString &path)
{
    if(this->mServer != nullptr)
        this->mServer->deleteLater();

    this->mServer = new QTcpServer;
    connect(this->mServer, &QTcpServer::newConnection, this, &Server::newConnection);
    if(!this->mServer->listen(QHostAddress::Any, port))
        emit error(tr("Could not start to listen on port!"));

    mPath = path;
}

void Server::stop()
{
    this->mServer->close();
    this->mServer->deleteLater();
    this->mServer = nullptr;
}

QString Server::currPath() const
{
    return this->mPath;
}

void Server::newConnection()
{
    auto socket = this->mServer->nextPendingConnection();

    // First, read client request
    socket->waitForReadyRead();
    QString request = socket->readAll();

    // Extract from the request the path to read
    auto beginUri = request.indexOf(" ") + 1;
    auto endUri = request.indexOf(" ", beginUri);
    QString URI = request.mid(beginUri, endUri - beginUri);

    if(URI.contains('?')) {
        URI = URI.left(URI.indexOf('?'));
    }

    URI = QUrl(URI).toString();

    // Process request
    // Determine the path of the requested file
    QString requestedPath = QDir(currPath()).absoluteFilePath(URI.mid(1));

    qDebug("REQUEST: %s (%s)", URI.toStdString().c_str(), requestedPath.toStdString().c_str());

    // Check if it is built-in file
    if(URI == "/" ASSETS_PATH "style.css") {
        QFile f(":/assets/style.css");
        f.open(QIODevice::ReadOnly);
        sendResponse(socket, &f, 200, "text/css");
        f.close();
        return;
    }

    if(URI == "/" ASSETS_PATH "folder.png") {
        QFile f(":/assets/folder.png");
        f.open(QIODevice::ReadOnly);
        sendResponse(socket, &f, 200, "image/png");
        f.close();
        return;
    }

    if(URI == "/" ASSETS_PATH "file.png") {
        QFile f(":/assets/document_black.png");
        f.open(QIODevice::ReadOnly);
        sendResponse(socket, &f, 200, "image/png");
        f.close();
        return;
    }

    if(URI == "/" ASSETS_PATH "back.png") {
        QFile f(":/assets/arrow_undo.png");
        f.open(QIODevice::ReadOnly);
        sendResponse(socket, &f, 200, "image/png");
        f.close();
        return;
    }

    // Check if file exists
    QFileInfo info(requestedPath);

    if(!info.exists()) {
        sendResponse(socket, DEFAULT_STYLESHEET "<h1>404 Not Found !</h1><p>Requested path not found: <i>" + requestedPath+"</i></p>", 404);
        return;
    }


    // Check if it is a directory
    if(info.isDir()) {

        // Check if URL ends with a slash
        if(!URI.endsWith("/")){

            // Redirect to the appropriate location
            QString newURI = URI + "/";
            QMap<QString, QString> headers;
            headers["Location"] = newURI;
            sendResponse(socket, "<p>Page has moved here: " + newURI + "</p>", 301, "text/html", headers);

            return;
        }

        // Check for index.html
        auto potentialIndex = QDir(requestedPath).absoluteFilePath("index.html");
        if(QFile(potentialIndex).exists()) {
            requestedPath = potentialIndex;
            info = QFileInfo(potentialIndex);
        }

        // Give directory listing
        else {

            QString response = QString("<!DOCTYPE html><html><head><title>")
                    + "Index of " + URI + "</title>"
                    + DEFAULT_STYLESHEET
                    + "</head>\n<body>"
                    + "<h1>Index of " + URI + "</h1>\n"
                    + "<ul>\n";

            if(URI != "/")
                    response += "<li><a href=\"..\"><img src=\"" ASSETS_URL "back.png\" /> Parent directory</a></li>\n";

            // Get file content
            for(auto f : QDir(requestedPath).entryList()) {

                if(f.startsWith("."))
                    continue;

                QFileInfo info(QDir(requestedPath).absoluteFilePath(f));

                response += "<li><a href=\"" + f + "\"> <img src=\"" ASSETS_URL + (info.isDir() ? "folder" : "file")
                        + ".png\" />  "+f+"</a></li>\n";
            }

            response += "</ul></body></html>";

            sendResponse(socket, response);

            return;
        }
    }

    // ... else it is a file
    QFile file(info.absoluteFilePath());
    QString mimeType = QMimeDatabase().mimeTypeForFile(info.absoluteFilePath()).name();


    if(!file.open(QIODevice::ReadOnly)) {
        sendResponse(socket, DEFAULT_STYLESHEET "File could not be opened for reading!", 500);
        return;
    }


    sendResponse(socket, &file, 200, mimeType);
    file.close();
}

void Server::sendResponse(QTcpSocket *socket, const QString &data, int code, const QString mimeType, QMap<QString, QString> headers)
{
    QByteArray arr(data.toStdString().c_str(), data.size());
    sendResponse(socket, arr, code, mimeType, headers);
}

void Server::sendResponse(QTcpSocket *socket, QByteArray &data, int code, const QString mimeType, QMap<QString, QString> headers)
{
    QBuffer buffer(&data);
    buffer.open(QIODevice::ReadOnly);
    sendResponse(socket, &buffer, code, mimeType, headers);
    buffer.close();
}

void Server::sendResponse(QTcpSocket *socket, QIODevice *device, int code, const QString mimeType, QMap<QString, QString> headers)
{
    // Start to write response
    QString head = "HTTP/1.1 " + QString::number(code) + " ";

    // Add HEAD
    switch(code) {
        case 200:
            head += "OK";
            break;

        case 301:
            head += "Moved Permanently";
            break;

        case 404:
            head += "Not Found";
            break;

        default:
            head += "Error";
    }

    head += "\n";

    headers["Server"] = "SimpleHTTPServer";
    headers["Content-Length"] = QString::number(device->size());
    headers["Content-Type"] = mimeType;
    headers["Connection"] = "close";

    // Add headers
    for(auto i = headers.constBegin(); i != headers.constEnd(); ++i) {
        head += i.key() + ": " + i.value() + "\n";
    }

    head += "\n";

    // Send response header
    socket->write(head.toStdString().c_str(), head.size());

    // Send response data
    while(device->bytesAvailable() > 0) {
        socket->write(device->read(BUFF_SIZE));

        while(socket->bytesToWrite() > 0)
            socket->waitForBytesWritten();
    }


    socket->close();

}
