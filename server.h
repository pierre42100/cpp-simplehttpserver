#pragma once

#include <QObject>
#include <QMap>

class QTcpServer;
class QTcpSocket;
class QIODevice;

class Server : public QObject
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = nullptr);

    /**
     * Request the server to be started
     *
     * @param port The port to listen on
     * @param path The target directory
     */
    void start(int port, const QString &path);

    void stop();

    QString currPath() const;

signals:
    void error(const QString &error);

public slots:

private slots:
    void newConnection();

private:

    void sendResponse(QTcpSocket *socket,
                      const QString &data,
                      int code = 200,
                      const QString mimeType = "text/html",
                      QMap<QString, QString> headers = QMap<QString, QString>());

    void sendResponse(QTcpSocket *socket,
                      QByteArray &data,
                      int code = 200,
                      const QString mimeType = "text/html",
                      QMap<QString, QString> headers = QMap<QString, QString>());

    void sendResponse(QTcpSocket *socket,
                      QIODevice *device,
                      int code = 200,
                      const QString mimeType = "text/html",
                      QMap<QString, QString> headers = QMap<QString, QString>());

    // Class members
    QTcpServer *mServer = nullptr;
    QString mPath;
};
