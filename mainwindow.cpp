#include <QFileDialog>
#include <QMessageBox>

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    updateUI(false);

    this->mServer = new Server();
    connect(this->mServer, &Server::error, this, &MainWindow::onError);
}

MainWindow::~MainWindow()
{
    delete ui;
}

QString MainWindow::currDir()
{
    return ui->pathEdit->text();
}

int MainWindow::currPort()
{
    return ui->portNumber->value();
}

void MainWindow::on_browseButton_clicked()
{
    auto newDir = QFileDialog::getExistingDirectory(
                this, tr("Select directory"), currDir());

    if(newDir.isEmpty())
        return;

    ui->pathEdit->setText(newDir);
}

void MainWindow::on_actionQuit_triggered()
{
    QApplication::quit();
}

void MainWindow::updateUI(bool serverIsStarted)
{
    ui->startButton->setEnabled(!serverIsStarted);
    ui->stopButton->setEnabled(serverIsStarted);
}

void MainWindow::on_startButton_clicked()
{
    this->mServer->start(currPort(), currDir());
    this->updateUI(true);
}

void MainWindow::onError(const QString &msg)
{
    this->updateUI(false);

    QMessageBox::warning(
                this, tr("Server error"), msg);
}

void MainWindow::on_stopButton_clicked()
{
    this->updateUI(false);

    this->mServer->stop();
}
